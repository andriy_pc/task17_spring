
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import first.configurations.Config2;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("insert which sub task you want to check:");
        System.out.println("0 - exit");
        System.out.println("1 - first");
        System.out.println("2 - second");
        int choice = in.nextInt();

        while(choice != 0) {
            if(choice == 1) {
                first.App.main();
            } else if (choice == 2) {
                second.App.main();
            } else {
                throw new RuntimeException("Wrong choice!");
            }
            System.out.println("insert which sub task you want to check:");
            System.out.println("0 - exit");
            System.out.println("1 - first");
            System.out.println("2 - second");
            choice = in.nextInt();
        }
    }
}
