package second.configs;

import org.springframework.context.annotation.*;
import org.springframework.context.annotation.ComponentScan.Filter;
import second.beans3.BeanE;

/*If a given profile is prefixed with '!',
  the annotated component will be registered
  if the profile is not active*/

@Profile("!conf2")
@Configuration
@ComponentScan(value={"second.beans2", "second.beans3"},
excludeFilters={@Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {BeanE.class})})
public class Config2 {

    @Profile(value={"custom", "!conf2"})
    @Bean(name="beanE")
    public BeanE getBeanE() {
        System.out.println("Creating BeanE through @Bean");
        return new BeanE();
    }

}
