package second.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@Profile("conf1")
@Configuration
@ComponentScan(value={"second.beans1", "second.other"})
//BeanA, BeanB, BeanC depends on beans in second.other package

public class Config1 {
}
