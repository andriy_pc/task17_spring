package second.beans3;
import org.springframework.stereotype.Component;

@Component
public class BeanE {
    public String toString() {
        return "BeanE";
    }
}
