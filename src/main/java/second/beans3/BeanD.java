package second.beans3;
import org.springframework.stereotype.Component;

@Component
public class BeanD {
    public String toString() {
        return "BeanD";
    }
}
