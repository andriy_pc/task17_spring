package second.beans3;
import org.springframework.stereotype.Component;

@Component
public class BeanF {
    public String toString() {
        return "BeanF";
    }
}
