package second.beans2;
import org.springframework.stereotype.Component;

@Component
public class RoseFlower {
    public String toString() {
        return "RoseFlower";
    }
}
