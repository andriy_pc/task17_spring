package second.beans2;
import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {
    public String toString() {
        return "NarcissusFlower";
    }
}
