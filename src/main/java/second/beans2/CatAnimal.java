package second.beans2;
import org.springframework.stereotype.Component;

@Component
public class CatAnimal {
    public String toString() {
        return "CatAnimal";
    }
}
