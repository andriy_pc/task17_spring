package second.beans1;

import org.springframework.stereotype.Component;
import second.other.OtherBeanC;

@Component
public class BeanC {

    OtherBeanC otherBeanC1;
    OtherBeanC otherBeanC2;

    public BeanC(OtherBeanC c1, OtherBeanC c2) {
        otherBeanC1 = c1;
        otherBeanC2 = c2;
    }

    public String toString() {
        return "BeanC: auto wired constructor(\n\totherBeanC1: " +
                otherBeanC1 + "\n\totherBeanC2: " +
                otherBeanC2+");";
    }
}
