package second.beans1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import second.other.OtherBeanA;

@Component
public class BeanA {
    @Autowired
    OtherBeanA otherBeanA1;
    @Autowired
    OtherBeanA otherBeanA2;
    public String toString() {
        return "BeanA: auto wired field(\n\totherBeanA1: " +
                otherBeanA1 + "\n\totherBeanA2: " +
                otherBeanA2+");";
    }
}