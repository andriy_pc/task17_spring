package second.beans1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import second.other.OtherBeanB;

@Component
public class BeanB {
    OtherBeanB otherBeanB1;
    OtherBeanB otherBeanB2;
    @Autowired
    public void setOtherBeanB(OtherBeanB o1, OtherBeanB o2) {
        otherBeanB1 = o1;
        otherBeanB2 = o2;
    }

    public String toString() {
        return "BeanB: auto wired setter(\n\totherBeanB1: " +
                otherBeanB1 + "\n\totherBeanB2: " +
                otherBeanB2+");";
    }
}
