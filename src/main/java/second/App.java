package second;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;
import second.beans1.BeanA;
import second.beans1.BeanB;
import second.beans1.BeanC;
import second.configs.Config1;
import second.configs.Config2;
import second.part.Car;
import second.part.ConfigCars;
import second.part.Garage;
import second.part.ParkLot;

public class App {
    private static Logger log = LogManager.getLogger();
    public static void main(String... args) {
        log.trace("------second sub task------");
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "conf1");
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        Config1.class, Config2.class, ConfigCars.class);

        log.info("Beans in ApplicationContext:");
        for(String s : context.getBeanDefinitionNames()) {
            System.out.print("\t");
            System.out.println(s);
        }

        log.info("Using @Autowired to make dependency injection:");
        BeanA a = context.getBean(BeanA.class);
        System.out.println("\t"+a);
        BeanB b = context.getBean(BeanB.class);
        System.out.println("\t"+b);
        BeanC c = context.getBean(BeanC.class);
        System.out.println("\t"+c);

        log.info("Injection list of Car:");
        ParkLot pl = context.getBean(ParkLot.class);
        for(Car car : pl.getList()) {
            System.out.print("\t");
            System.out.println(car);
        }

        log.info("Injection Car objects with @Primary and @Qualifier:");
        Garage g = context.getBean(Garage.class);
        System.out.println(g);

    }
}
