package second.part;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Qualifier("wife")
public class Car3 implements Car {
    public String toString() {
        return "wife's car3(Order=1)";
    }
}
