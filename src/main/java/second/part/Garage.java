package second.part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Garage {
    @Autowired
    private Car primaryCar;

    @Autowired
    @Qualifier("second")
    private Car secondaryCar;

    @Autowired
    @Qualifier("wife")
    private Car wifesCar;

    public String toString() {
        return "\tprimaryCar: " + primaryCar +
                "\n\tsecondaryCar: " + secondaryCar +
                "\n\twife's car: " + wifesCar;
    }
}
