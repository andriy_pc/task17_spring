package second.part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ParkLot {
    @Autowired
    private List<Car> cars;

    public List<Car> getList() {
        return cars;
    }
}
