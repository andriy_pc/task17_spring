package second.part;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Primary
@Component
@Order(2)
public class Car2 implements Car {
    public String toString() {
        return "primary car2(Order=2)";
    }
}
