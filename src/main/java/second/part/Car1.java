package second.part;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(3)
@Qualifier("second")
public class Car1 implements Car {
    public String toString() {
        return "second car1(Order=3)";
    }
}
