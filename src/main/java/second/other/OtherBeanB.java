package second.other;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanB {
    @Override
    public String toString() {
        return "other beanB(singleton); hash code:" + hashCode();
    }
}
