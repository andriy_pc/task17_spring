package second.other;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class OtherBeanA {
    @Override
    public String toString() {
        return "other beanA(prototype); hash code:" + hashCode();
    }
}
