package first.validation;

public interface BeanValidator {
    boolean validate();
}
