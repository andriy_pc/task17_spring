package first;

import first.configurations.Config2;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class App {
    private static Logger log = LogManager.getLogger();

    public static void main(String... args) {
        log.trace("------first sub task------");
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Config2.class);

        System.out.println("context bean list: ");
        for(String s : context.getBeanDefinitionNames()) {
            System.out.print("\t");
            System.out.println(s);
        }
        log.trace("before closing");
        context.close();
    }
}
