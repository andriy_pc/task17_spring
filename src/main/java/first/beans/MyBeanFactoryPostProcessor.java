package first.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    private static Logger log = LogManager.getLogger();

    @Override
    public void postProcessBeanFactory
            (ConfigurableListableBeanFactory configurableListableBeanFactory)
            throws BeansException {

        BeanDefinition beanDef;
        String name;

        String[] beans =
                configurableListableBeanFactory
                        .getBeanDefinitionNames();
        log.info("BeanFactoryPostProcessor: get properties of all beans");
        for(String s : beans) {
            name = s;
            if(name.equals("BeanB")) {
                beanDef =
                        configurableListableBeanFactory
                        .getBeanDefinition(name);
                log.warn("*changing initialization method of BeanB*");
                beanDef.setInitMethodName("changedInit");
            }
            System.out.println("\t"+configurableListableBeanFactory.getBeanDefinition(name));
        }
    }
}
