package first.beans;

import first.validation.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Bean implements BeanValidator {
    private static Logger log = LogManager.getLogger();

    private int value;
    private String name;
    @Override
    public boolean validate() {
        return (value >= 0) && (name != null);
    }

    public void setValue(int v) {
        value = v;
    }

    public void setName(String n) {
        name = n;
    }

    public void init() {
        log.info("initiating: " + this);
    }

    public void destroy() {
        log.info("destroying: " + this);
    }

    public String toString() {
        return getClass().getSimpleName()+"{" + name + ": " + value + "}";
    }
}
