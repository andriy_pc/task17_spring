package first.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {
    private static Logger log = LogManager.getLogger();

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
       if(bean instanceof Bean) {
           log.info("Validating bean: " + bean.getClass().getSimpleName());
           if(!((Bean) bean).validate()) {
               throw new RuntimeException("Invalid bean!");
           }
       }
        return bean;
    }
}
