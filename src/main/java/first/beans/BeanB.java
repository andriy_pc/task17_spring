package first.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanB extends Bean {
    private static Logger log = LogManager.getLogger();
    public void changedInit() {
        log.info("BeanB changedInit()");
    }
}
