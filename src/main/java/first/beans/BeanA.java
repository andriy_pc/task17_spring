package first.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
public class BeanA extends Bean implements InitializingBean, DisposableBean {
    private static Logger log = LogManager.getLogger();

    private Bean[] beans = new Bean[2];
    public void setBeans(Bean b1, Bean b2) {
        beans[0] = b1;
        beans[1] = b2;
    }
    public String toString() {
        return "\t" + getClass().getSimpleName() + "\n\tbeans: " +
                beans[0] +
                "," + beans[1];
    }

    @PostConstruct
    private void initA() {
        log.info("Initiating A with PostConstruct");
    }
    
    @Override
    public void afterPropertiesSet() {
        log.info("BeanA: InitializingBean afterPropertiesSet"+this);
    }

    @Override
    public void destroy() {
        log.info("BeanA: Disposable destroy()"+this);
    }
}
