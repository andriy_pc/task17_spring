package first.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class BeanE extends Bean {
    private static Logger log = LogManager.getLogger();

    private Bean beanA;
    public void setBeanA(Bean a) {
        beanA = a;
    }
    public String toString() {
        return "\t" + super.toString() + "\nbeanA: " + beanA;
    }

    @PostConstruct
    private void initE() {
        log.info("PostConstruct" + this);
    }

    @PreDestroy
    public void destroy() {
        log.info("PreDestroy: " + this);
    }
}
