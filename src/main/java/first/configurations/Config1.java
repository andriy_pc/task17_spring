package first.configurations;

import first.beans.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("p1.properties")
public class Config1 {
    @Autowired
    Environment environment;


    @Bean(name = "BeanB",
            initMethod = "init",
            destroyMethod = "destroy")
    @DependsOn(value = {"BeanD"})
    public BeanB getBeanB() {
        BeanB beanB = new BeanB();
        beanB.setName(environment.getProperty("beanB.name"));
        beanB.setValue(Integer.parseInt(environment.getProperty("beanB.value")));
        return beanB;
    }

    @Bean(name = "BeanC",
            initMethod = "init",
            destroyMethod = "destroy")
    @DependsOn(value = {"BeanB"})
    public BeanC getBeanC() {
        BeanC beanC = new BeanC();
        beanC.setName(environment.getProperty("beanC.name"));
        beanC.setValue(Integer.parseInt(environment.getProperty("beanC.value")));
        return beanC;
    }

    @Bean(name = "BeanD",
            initMethod = "init",
            destroyMethod = "destroy")
    public BeanD getBeanD() {
        BeanD beanD = new BeanD();
        beanD.setName(environment.getProperty("beanD.name"));
        beanD.setValue(Integer.parseInt(environment.getProperty("beanD.value")));
        return beanD;
    }
}
