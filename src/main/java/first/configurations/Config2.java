package first.configurations;

import first.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import(Config1.class)
public class Config2 {

    @Bean(name="BeanABC")
    public BeanA getBeanA1(BeanB b, BeanC c) {
        BeanA beanA = new BeanA();
        beanA.setBeans(b, c);
        beanA.setName("BeanABC");
        beanA.setValue(1);
        return beanA;
    }

    @Bean(name="BeanABD")
    public BeanA getBeanA2(BeanB b, BeanD d) {
        BeanA beanA = new BeanA();
        beanA.setBeans(b, d);
        beanA.setName("BeanABD");
        beanA.setValue(1);
        return beanA;
    }

    @Bean(name="BeanACD")
    public BeanA getBeanA3(BeanC c, BeanD d) {
        BeanA beanA = new BeanA();
        beanA.setBeans(c, d);
        beanA.setName("BeanACD");
        beanA.setValue(1);
        return beanA;
    }

    @Bean(name="BeanE1A")
    public BeanE getBeanE1(@Qualifier("BeanABC")BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setBeanA(beanA);
        beanE.setName("BeanE");
        beanE.setValue(5);
        return beanE;
    }

    @Bean(name="BeanE2A")
    public BeanE getBeanE2(@Qualifier("BeanABD")BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setBeanA(beanA);
        beanE.setName("BeanE");
        beanE.setValue(5);
        return beanE;
    }

    @Bean(name="BeanE3A")
    public BeanE getBeanE3(@Qualifier("BeanACD")BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setBeanA(beanA);
        beanE.setName("BeanE");
        beanE.setValue(5);
        return beanE;
    }

    @Bean(name="BeanF")
    @Lazy
    public BeanF getBeanF() {
        BeanF beanF = new BeanF();
        return beanF;
    }

    @Bean
    public MyBeanFactoryPostProcessor getMyFactory() {
        return new MyBeanFactoryPostProcessor();
    }

    @Bean
    public MyBeanPostProcessor getMyBeanPostProcessor() {
        return new MyBeanPostProcessor();
    }
}
